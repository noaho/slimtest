<?php
require "bootstrap.php";
use Chatter\Models\User;

$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['cnumber'].', and your product is '.$args['pnumber']);
});

//view
$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();

    $payload = [];
    foreach($users as $u){
        $payload[$u->id] = [
            "id" =>$u->id,
            "name" => $u->name,
            "phonenumber" => $u->phonenumber
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//create
$app->post('/users', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');//what the user puts in 
    $phonenumber = $request->getParsedBodyParam('phonenumber','');//what the user puts in 
    $_user = new User();
    $_user->name = $name;//what the user puts in must match to my coulmns in the table
    $_user->phonenumber = $phonenumber;
    $_user->save();
    
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//delete
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

//update
$app->put('/users/{user_id}', function($request, $response, $args){
    $phonenumber = $request->getParsedBodyParam('phonenumber','');
    $name = $request->getParsedBodyParam('name','');
    $_user = User::find($args['user_id']);
    //שדות בטבלה
    $_user->phonenumber = $phonenumber;
    $_user->name = $name;
   
    if($_user->save()){
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});
//for updating a user
$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = User::find($_id);
    return $response->withStatus(200)->withJson($message);
});

//create a few users togetHER (bulk)
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});
/////////////////////////////////////////////////
//login 
$app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});
///////////////////////////////////////////////
//הרשאה לאבטחת מידע same origin policy

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

//access for angular to slim
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();